from flask import Flask, render_template, request, jsonify
import json, requests

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def chatbot():
    if request.method == 'POST':
        text = str(request.form.get('sentence'))
        payload = json.dumps({"sender": "Rasa","message": text})
        headers = {'Content-type': 'application/json', 'Accept':     'text/plain'}
        response = requests.request("POST",   url="http://localhost:5005/webhooks/rest/webhook", headers=headers, data=payload)
        response = response.json()[0]['text']
        return render_template('index.html', response=response)
    else:
        return render_template('index.html')

if __name__ == "__main__":
    app.run(host="localhost", port='8080')